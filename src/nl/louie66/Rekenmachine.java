package nl.louie66;

import java.util.Scanner;

public class Rekenmachine {
    /**
     * Dit is een programma waarmee eenvoudige sommen oplost kunnen worden
     *
     * @author Rene de Leeuw
     */
    public static void main(String[] args) {

        // De waarde 'x' in operator zorgt dat the while loop gaat lopen
        char operator = 'x';

        // Standard input object
        Scanner input = new Scanner(System.in);

        // Deze loop blijft doorlopen als de operator niet geldig is.
        // Dus geen -,+,/,*,% maar ook 's' en  'S' om de loop te stoppen
        while (!isGeldigeOperator(operator)) {

            // Het eerst ingevoerde teken wordt opgeslagen in de operator variabele
            System.out.print("Operator (S = stoppen): ");
            operator = input.next().charAt(0);

            // als de operator 's' of 'S' is stopt de loop en het programma
            if (operator == 'S' || operator == 's')
                break;

            // Doe alleen een som als de operator geldig is met de
            // isGeldigeOperator wordt dit gecontroleerd
            if (isGeldigeOperator(operator)) {

                // Er kunnnen alleen maar getallen ingevoerd worden
                System.out.print("Eerste getal: ");
                double getal1 = input.nextDouble();
                System.out.print("Tweede getal: ");
                double getal2 = input.nextDouble();

                // Print de som met behulp van de printSom methode
                printSom(operator, getal1, getal2);
            } else {
                System.out.println("Operator is ongeldig\n");
            }

            // Maak de operator ongeldig om de loop te kunnen continueren en de
            // volgende operator te vragen en een nieuwe som te kunnen printen
            operator = 'x';
        }
    }

    /**
     * Methode om de geldigheid van de operateor te controleren
     *
     * @param karakter van type char kan waarde
     * @return een boolean waarde om aan tegeven dat de operator
     * geldis is ('s','S','-','+','*','/' of '%' ) of niet
     */
    private static boolean isGeldigeOperator(char karakter) {
        return karakter == '-' || karakter == '+' || karakter == '*' ||
                karakter == '/' || karakter == '%' || karakter == 's' ||
                karakter == 'S';
    }

    /**
     * Methode om de som uit te rekenen en naar de console te printen
     *
     * @param operator char geldige operator
     * @param getal1   double linker lid van de som
     * @param getal2   double rechter lid van de som
     */
    private static void printSom(char operator, double getal1, double getal2) {
        double uitkomst = 0;

        switch (operator) {
            case '+':
                uitkomst = getal1 + getal2;
                break;
            case '-':
                uitkomst = getal1 - getal2;
                break;
            case '/':
                uitkomst = getal1 / getal2;
                break;
            case '*':
                uitkomst = getal1 * getal2;
                break;
            case '%':
                uitkomst = getal1 % getal2;
                break;
        }
        System.out.printf("%s %s %s = %s\n", getal1, operator, getal2, uitkomst);
        System.out.println();
    }
}
